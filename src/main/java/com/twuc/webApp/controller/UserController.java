package com.twuc.webApp.controller;


import com.twuc.webApp.entity.Message;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {


    @GetMapping("/api/no-return-value-with-annotation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void getValue() {

    }


    @GetMapping("/api/messages/{message}")
    public String getMessage(@PathVariable String message) {
        return message;
    }

    @GetMapping("/api/message-objects/{message}")
    public Message getMessageObject(@PathVariable String message) {
        return new Message(message);
    }

    @GetMapping("/api/message-objects-with-annotation/{message}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Message getMessageObjectWithStatus(@PathVariable String message) {
        return new Message(message);
    }


    @GetMapping("/api/message-entities/{message}")
    public ResponseEntity<Message> getMessageEntityWithStatus(@PathVariable String message) {
        return ResponseEntity.status(HttpStatus.OK)
                .header("X-Auth", "me")
                .body(new Message(message));
    }

    @GetMapping("/api/get/exception")
    public void getException(){
        throw new RuntimeException();
    }



}
