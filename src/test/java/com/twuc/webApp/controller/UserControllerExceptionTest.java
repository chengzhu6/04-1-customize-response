package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class UserControllerExceptionTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void should_return_500() {
        ResponseEntity<Void> forEntity = testRestTemplate.getForEntity("/api/get/exception", Void.class);
        assertEquals(forEntity.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}