package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    void should_return_204_when_specify_the_response_code() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/no-return-value-with-annotation"))
                .andExpect(MockMvcResultMatchers.status().is(204));
    }

    @Test
    void should_return_200_and_message() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/messages/good"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("good"))
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN_VALUE));
    }


    @Test
    void should_return_200_and_message_object() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/message-objects/good"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.value").value("good"))
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    void should_return_202_and_message_object() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/message-objects-with-annotation/good"))
                .andExpect(MockMvcResultMatchers.status().is(202))
                .andExpect(MockMvcResultMatchers.jsonPath("$.value").value("good"))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }


    @Test
    void should_return_specify_respones_entity() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/message-entities/good"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.value").value("good"))
                .andExpect(MockMvcResultMatchers.header().string("X-Auth", "me"))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }


    @Test
    void shoule_throw_exception() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/get/exception"))
                .andExpect(MockMvcResultMatchers.status().is(500));
    }

}